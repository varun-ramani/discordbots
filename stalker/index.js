const Discord = require('discord.js');
var ioOperations = require('./serialize');
var moment = require('moment');
const client = new Discord.Client();

var storedData = ioOperations.deSerialize();

client.on('ready', () => {
    console.log(`Logged in as user ${client.user.tag}`);
});

client.on('message', message => {
    if (message.content.includes(client.user)) {
        message.channel.send("Hello! I'm Stalker, a very scary stalkerbot.");
    } else {

        if (!(message.guild in storedData)) {
            storedData[message.guild] = {};
        }

        if (!(message.channel.name in storedData[message.guild])) {
            storedData[message.guild][message.channel.name] = [];
        }

        storedData[message.guild][message.channel.name].push(`${message.author.username}: ${message.content} |<>| ${moment().format('MM/DD/YYYY-h:mm a')}`);

    }

    ioOperations.serialize(storedData);
});

client.login('NTg4NTA3MTAzMjcwOTI4NDE4.XQGIDA.fySduTX3-w8xsEy5-v-PnuqZXK0');
