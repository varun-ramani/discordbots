const Discord = require('discord.js');
var ioOperations = require('./serialize');
const client = new Discord.Client();

var storedData = ioOperations.deSerialize();


client.on('ready', () => {
    console.log(`Logged in as user ${client.user.tag}`);
});

var checkAccount = (message) => {
    if (!(message.guild.id in storedData)) {
        storedData[message.guild.id] = {};
        message.reply(`since your server ${message.guild} is not registered with me, I just registered it for you.`);
    }
    if (!(message.author in storedData[message.guild.id])) {
        message.reply(`since this is your first time using the profile feature, I have made you a new account.`)
        storedData[message.guild.id][message.author] = {
            "resources": {
                "money": 0,
                "trees": 0,
                "forests": 0
            }
        }
    }
}

client.on('message', message => {
    console.log(`${message.author.username} just sent "${message.content}"!`);
    if (message.content.startsWith("yeebot.")) {
        var content = message.content.replace("yeebot.", "");
        if (content.startsWith("say_hello")) {
            message.channel.send(`Hello, ${message.author}!`);
        } else if (content.startsWith("send_custom_message ")) {
            var content = content.replace("send_custom_message ", "");
            message.channel.send(content);
        } else if (content.startsWith("send_help_please")) {
            message.author.send("Hey there!");
            message.author.send("Here's some documentation for you:");
            message.author.sendCode("text", `
yeebot.send_help:
Help user

yeebot.send_help_please:
Show this help menu

yeebot.send_custom_message <message>:
Send a custom message of your choosing

yeebot.say_hello:
Hey there!

yeebot.steal_money:
Steal one money from Yee Central

yeebot.transfer_funds <otherusertag> <numerical amount> <currency (trees, money, or forests)>
Transfer funds to another another user

`);
        } else if (content.startsWith("send_help")) {
            message.author.send("Sike no instructions for you! Be a polite boi and use $yeebot.send_help_please if you want instructions.");
            message.channel.send(`I sent you instructions in your DMs, ${message.author}`);
        } else if (content.startsWith("weather")) {
            message.channel.send("Weather not implemented yet");
        } else if (content.startsWith("my_profile")) {
            checkAccount(message);
            message.channel.send(`${message.author}'s statistics:`);
            var senderdata = storedData[message.guild.id][message.author];
            message.channel.sendCode("text", `
Money: ${senderdata.resources.money}
Trees: ${senderdata.resources.trees}
Forests: ${senderdata.resources.forests}
            `);

            ioOperations.serialize(storedData);

            console.log(storedData);
        } else if (content.startsWith("steal_money")) {
            checkAccount(message);
            if (Math.floor((Math.random() * 5)) != 1) {
                storedData[message.guild.id][message.author].resources.money += 1;
                message.channel.send(`${message.author} successfully stole 1 money from Yee Central.`);
            } else {
                switch (Math.floor(Math.random() * 5)) {
                    default:
                        message.channel.send(`${message.author} was caught by the Yee Police and did not successfully complete the theft. Luckily, they let him off with a heavy rebuke and a promise never to steal again. (lol they really think that he's going to keep that promise lmao)`);
                        break;
                    case 2:
                        message.channel.send(`${message.author} was caught and assaulted by the Yee Police. One of the Yee Police guards accidentally touched ${message.author} on the buttcheek and then ${message.author} was able to join the #MeToo movement and file a sexual assault claim. Yee Central paid a fine of 3 money to ${message.author}`);
                        storedData[message.guild.id][message.author].resources.money += 3;
                        break;
                    case 3:
                        message.channel.send(`${message.author} was caught by the Yee Police as they were trying to break into the top secret Yee Vault within Yee Central in order to steal 10 money. The guards demanded a bribe of 20% of all of ${message.author}'s money.`);
                        storedData[message.guild.id][message.author].resources.money = Math.floor(storedData[message.guild.id][message.author].resources.money * 0.8);
                        break;
                    case 4:
                        message.channel.send(`${message.author} was caught by the Yee Police as they were trying to break into the top secret Yee Vault within Yee Central in order to steal 10 money. The guards demanded a hefty bribe of 50% of all of ${message.author}'s money.`);
                        storedData[message.guild.id][message.author].resources.money = Math.floor(storedData[message.guild.id][message.author].resources.money * 0.5);
                        break;
                }
            }

            ioOperations.serialize(storedData);
        } else if (content.startsWith("transfer_funds")) {
            content = content.replace("transfer_funds ", "");
            var args = content.split(" ");
            console.log(args);
            if (args.length != 3) {
                message.reply("that set of parameters is either too short or too long.");
            } else {
                if (!(args[0] in storedData[message.guild.id])) {
                    message.channel.send(`${args[0]} is either not a valid user tag or never made a profile.`);
                } else if (isNaN(parseInt(args[1]))) {
                    message.channel.send(`"${args[1]}" is not an integer value.`);
                } else if (!(args[2] in storedData[message.guild.id][message.author].resources)) {
                    message.channel.send(`"${args[2]}" is not a valid form of currency.`);
                } else {
                    if (storedData[message.guild.id][message.author]["resources"][args[2]] < parseInt(args[1])) {
                        message.channel.send(`Lol ${message.author} doesn't even have ${args[1]} ${args[2]}. Transfer invalid.`);
                    } else {
                        storedData[message.guild.id][message.author]["resources"][args[2]] -= parseInt(args[1]);
                        storedData[message.guild.id][args[0]]["resources"][args[2]] += parseInt(args[1]);
                        message.channel.send(`${message.author} successfully transferred ${args[1]} ${args[2]} to ${args[0]}`);
                    }
                }
            }

            ioOperations.serialize(storedData);
        } else if (content.startsWith("spam")) {
            content = content.replace("spam ", "");
            var index = content.indexOf(" ");
            var count = parseInt(content.substring(0, index));
            content = content.substring(index + 1, content.length);
            if (count > 50) {
                message.channel.send(`Nah screw you ${message.author}, I'm feeling too lazy`);
            }
            else {
                for (var i = 0; i < count; i++) {
                    message.channel.send(content);
                }
            }
        } else {
            message.reply("you sent an invalid command!");
        }
    }
});

client.login('MzM4MTM1NjQxMTg1NTgzMTA2.XNyyYw.TEQKpw2lGHTlPj-55JFViqVI3j8');