var fs = require('fs');
var shell = require('shelljs');

var dataFilePath = process.env['HOME'] + '/.config/yeebot/data.json'

if (!(fs.existsSync(dataFilePath))) {
    console.log("Data JSON file not found, creating now");
    shell.mkdir('-p', "~/.config/yeebot")
    shell.touch(dataFilePath);
}

var serialize = (jsonObject) => {
    fs.writeFile(dataFilePath, JSON.stringify(jsonObject), err => {
        if (err) console.log(err);
    });
}

var deSerialize = () => {
    var buffer = fs.readFileSync(dataFilePath, "utf8");
    if (buffer === "") {
        return JSON.parse("{}");
    } else {
        return JSON.parse(buffer);
    }
}

module.exports.serialize = serialize;
module.exports.deSerialize = deSerialize;